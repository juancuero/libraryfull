package com.example.LibraryFull.controller;

import com.example.LibraryFull.exception.ResourceNotFoundException;
import com.example.LibraryFull.entity.User;
import com.example.LibraryFull.repository.UserRepository;
import com.example.LibraryFull.request.*;
import com.example.LibraryFull.response.UserIdentityAvailability;
import com.example.LibraryFull.response.UserSummary;
import com.example.LibraryFull.security.UserPrincipal;
import com.example.LibraryFull.security.CurrentUser;
import com.example.LibraryFull.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;


    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName(),currentUser.getEmail());
        return userSummary;
    }

    



   

}
