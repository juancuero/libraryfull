package com.example.LibraryFull.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LibraryFull.request.EditorialRequest;
import com.example.LibraryFull.response.PagedResponseWithEntity;
import com.example.LibraryFull.response.BookAuthorsResponse;
import com.example.LibraryFull.response.BookPlainResponse;
import com.example.LibraryFull.response.EditorialResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.service.EditorialService;
import com.example.LibraryFull.util.AppConstants;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@Api(tags = "Editorial Endpoint",value = "Editorial Management System", description = "Operations pertaining to editorial in Library Management System")
@RequestMapping(path = "/editorials",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class EditorialController {
	
	@Autowired
	EditorialService editorialService;
	
	@ApiOperation(value = "Find all Editorials",notes = "Return all Editorials",authorizations = { @Authorization(value="Bearer") })
	@GetMapping
	public ResponseEntity<PagedResponse<EditorialResponse>> getEditorials(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		return ResponseEntity.ok(editorialService.getEditorials(page, size));
		
	}
	
	@ApiOperation(value = "Create new editorial", notes = "Creates new  editorial. Returns created editorial with id.",authorizations = { @Authorization(value="Bearer") },response = EditorialResponse.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EditorialResponse> createAuthor(@Valid @RequestBody EditorialRequest editorialRequest){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(editorialService.createEditorial(editorialRequest));
	}
	
	@ApiOperation(value = "Get editorial by id", notes = "Returns editorial for id specified.",response = EditorialResponse.class)
	@GetMapping("/{editorialId}")
	public ResponseEntity<EditorialResponse> getAuthors(
			@PathVariable(value = "editorialId") Long editorialId) {
		
		return ResponseEntity.ok(editorialService.getEditorialById(editorialId));
	}
	
	@ApiOperation(value = "Find all books by editorial",notes = "Retrieve All Books By Editorial")
	@GetMapping("/{editorialId}/books")
	public ResponseEntity<PagedResponseWithEntity<EditorialResponse,BookAuthorsResponse>> getBooksByEditorial(
			@PathVariable(value = "editorialId") Long editorialId,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		return ResponseEntity.ok(editorialService.getBooksByEditorial(editorialId,page, size));
		
	}

}
