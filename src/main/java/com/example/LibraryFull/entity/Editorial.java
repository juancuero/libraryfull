package com.example.LibraryFull.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


import lombok.Data;

@Data
@Entity
@Table(name = "editorials")
public class Editorial {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull(message = "Please provide a name")
	private String name;
	@NotNull(message = "Please provide a description")
	private String description;
	@NotNull(message = "Please provide year")
	private int fundation;
	@NotNull(message = "Please provide website")
	private String website;
	@OneToMany(mappedBy = "editorial", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Book> books;
}