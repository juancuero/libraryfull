package com.example.LibraryFull.entity;

public enum RoleName {
	ROLE_USER,
    ROLE_ADMIN
}
