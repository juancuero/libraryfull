package com.example.LibraryFull.entity;

public enum StatusBorrow {
	PENDING,
	APPROVED,
	CANCELLED
}
