package com.example.LibraryFull.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "authors")
public class Author {
	
	@Id
    @GeneratedValue(generator = "author_generator")
    @SequenceGenerator(
            name = "author_generator",
            sequenceName = "author_sequence",
            initialValue = 1000
    )
    private Long id;

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;
    
    @ManyToMany(mappedBy = "authors")
    private Set<Book> books = new HashSet<>();

}
