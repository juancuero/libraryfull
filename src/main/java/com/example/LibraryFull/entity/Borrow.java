package com.example.LibraryFull.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import com.example.LibraryFull.entity.audit.UserDateAudit;

import lombok.Data;

@Data
@Entity
@Table(name = "borrows")
public class Borrow extends UserDateAudit {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "book_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Book book;
	
	
	@Temporal(TemporalType.DATE)
	private Date receptionDate;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT NULL")
	private Date deliveryDate = null;
	
	@Column(length = 32, columnDefinition = "varchar(32) default 'PENDING'")
	@Enumerated(value = EnumType.STRING)
	private StatusBorrow status = StatusBorrow.PENDING;
}
