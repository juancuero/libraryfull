package com.example.LibraryFull.service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.LibraryFull.entity.User;
import com.example.LibraryFull.exception.ResourceNotFoundException;
import com.example.LibraryFull.repository.UserRepository;
import com.example.LibraryFull.response.UserSummary;

@Service
public class UserService {
	
	@Autowired
    UserRepository userRepository;
	
	@Autowired
	ModelMapper modelMapper;
	
	public Set<UserSummary> getUserInId(Set<Long> ids) {
		Set<User> usersSet = userRepository.findByIdIn(ids);
		Type listType = new TypeToken<Set<UserSummary>>(){}.getType();
		Set<UserSummary> users = modelMapper.map(usersSet, listType);
		return users;
	}
	
	public UserSummary getUserById(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", userId));
		
		UserSummary userSummary = modelMapper.map(user, UserSummary.class);
		return userSummary;

    }

}
