package com.example.LibraryFull.repository;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.LibraryFull.entity.Book;
import com.example.LibraryFull.entity.StatusBorrow;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	Page<Book> findByEditorialId(Long editorialId, Pageable pageable);
	Page<Book> findByAvailable(Boolean available, Pageable pageable);
	//Page<Book> findByAuthorId(Long authorId, Pageable pageable);
	
	@Transactional
	@Modifying(clearAutomatically = true)
    @Query("UPDATE #{#entityName} b SET b.available = :available WHERE b.id IN :ids")
    int setAvailableInBooks(@Param("available") Boolean available, @Param("ids") Collection<Long> ids);
	
	@Transactional
	@Modifying(clearAutomatically = true)
    @Query("UPDATE #{#entityName} b SET b.available = :newAvailable WHERE b.available = :available")
    int setAvailableBooks(@Param("newAvailable") Boolean newAvailable,@Param("available") Boolean available);
}