package com.example.LibraryFull.response;

import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor 
@AllArgsConstructor
@Data
public  class AuthorPlainResponse {
	private Long id;
	private String name;
}
