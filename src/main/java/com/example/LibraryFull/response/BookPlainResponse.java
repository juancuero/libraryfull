package com.example.LibraryFull.response;

import java.util.Set;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Book Plain", description = "Book Plain")
public class BookPlainResponse {
	@ApiModelProperty(notes = "Id of the book", example = "1",position = 1)
	private Long id;
	@ApiModelProperty(notes = "Tittle of the book", example = "Norma",position = 2)
	private String title;
	@ApiModelProperty(notes = "Book is available", example = "true",position = 3)
	private Boolean available;
	@ApiModelProperty(notes = "Description", example = "La editorial colombiana Norma se especializa en literatura infantil y escolar.",position = 4)
	private String description;

}
