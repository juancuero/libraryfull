package com.example.LibraryFull.response;

import java.util.Set;

import lombok.Data;

@Data
public class AuthorResponse {
	
    private Long id;

    private String name;
    
    private Set<BookPlainResponse> books ;
}

